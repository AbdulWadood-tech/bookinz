import 'package:booking_app/Screens/attractions_screen.dart';
import 'package:booking_app/Screens/car_rental_screen.dart';
import 'package:booking_app/Screens/stays_screen.dart';
import 'package:booking_app/Screens/taxi_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                title: Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text('Bookinz'),
                  ),
                ),
                pinned: true,
                backgroundColor: Colors.blue[900],
                floating: true,
                actions: [
                  Icon(
                    Icons.chat_bubble_outline_outlined,
                    size: 22,
                  ),
                  SizedBox(width: 10),
                  Icon(
                    FontAwesome.bell,
                    size: 22,
                  ),
                  SizedBox(width: 15),
                ],
                bottom: TabBar(
                  indicatorPadding:
                      EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                  indicator: BoxDecoration(
                    color: Colors.grey.withOpacity(0.3),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  isScrollable: true,
                  tabs: [
                    Tab(
                      child: Row(
                        children: [
                          Icon(Icons.bed_rounded),
                          SizedBox(width: 5),
                          Text('Stays'),
                        ],
                      ),
                    ),
                    Tab(
                      child: Row(
                        children: [
                          Icon(Icons.car_rental_outlined),
                          SizedBox(width: 5),
                          Text('Car rental'),
                        ],
                      ),
                    ),
                    Tab(
                      child: Row(
                        children: [
                          Icon(Icons.local_taxi_outlined),
                          SizedBox(width: 5),
                          Text('Taxi'),
                        ],
                      ),
                    ),
                    Tab(
                      child: Row(
                        children: [
                          Icon(Entypo.network, size: 22),
                          SizedBox(width: 5),
                          Text('Attractions'),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
            children: <Widget>[
              StaysScreen(),
              CarRentalScreen(),
              TaxiScreen(),
              AttractionScreen(),
            ],
          ),
        ),
      ),
    );
  }
}
