import 'package:booking_app/Screens/bookings_screen.dart';
import 'package:booking_app/Screens/saved_screen.dart';
import 'package:booking_app/Screens/search_screen.dart';
import 'package:booking_app/Screens/profile_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';

class NavigatorScreen extends StatefulWidget {
  const NavigatorScreen({Key? key}) : super(key: key);

  @override
  _NavigatorScreenState createState() => _NavigatorScreenState();
}

class _NavigatorScreenState extends State<NavigatorScreen> {
  int currentIndex = 0;

  getScreen() {
    if (currentIndex == 0) {
      return SearchScreen();
    } else if (currentIndex == 1) {
      return SavedScreen();
    } else if (currentIndex == 2) {
      return BookingsScreen();
    } else {
      return ProfileScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        unselectedItemColor: Colors.black,
        selectedItemColor: Colors.blue,
        onTap: (int) {
          setState(() {
            currentIndex = int;
          });
        },
        currentIndex: currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text('Search'),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesome.heart_empty),
            title: Text('Saved'),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.business_center_outlined,
            ),
            title: Text('Bookings'),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            title: Text('Profile'),
          ),
        ],
      ),
      body: getScreen(),
    );
  }
}
