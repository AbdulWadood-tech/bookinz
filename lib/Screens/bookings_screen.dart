import 'package:booking_app/Components/sign_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';

class BookingsScreen extends StatelessWidget {
  const BookingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your trips'),
        backgroundColor: Colors.blue[900],
        actions: [
          Icon(Icons.add),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 60),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
            ),
            Icon(
              FontAwesome.edge,
              size: 170,
              color: Colors.yellow[800],
            ),
            SizedBox(height: 20),
            Text(
              'Sign in for savings and more',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20),
            Text(
              'Sign in to see your bookings, plus unlock Genius\ndiscounts at thousands of properties worldwide.\nYou can also import individual bookings with your\n confirmation number and pin',
              style: TextStyle(fontSize: 17),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            SignInButton(),
            SizedBox(height: 40),
            Text(
              'Import booking',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.blue[700]),
            ),
          ],
        ),
      ),
    );
  }
}
