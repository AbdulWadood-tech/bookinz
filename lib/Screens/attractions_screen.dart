import 'package:booking_app/Components/covered_row.dart';
import 'package:booking_app/Components/horizental_divider.dart';
import 'package:booking_app/Components/top_destination_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';

class AttractionScreen extends StatefulWidget {
  const AttractionScreen({Key? key}) : super(key: key);

  @override
  _AttractionScreenState createState() => _AttractionScreenState();
}

class _AttractionScreenState extends State<AttractionScreen> {
  int selectedTab = 0;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 9,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 70, horizontal: 35),
                child: Column(
                  children: [
                    Text(
                      'Find and book a great experience',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 33),
                    ),
                    SizedBox(height: 14),
                    Text(
                      'Discover more of your destination and make the mose of your trip',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                    SizedBox(height: 30),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 13, horizontal: 13),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.white,
                        border: Border.all(
                            color: Colors.orange.withOpacity(0.6), width: 4),
                      ),
                      child: Row(
                        children: [
                          Icon(
                            Icons.search,
                            color: Colors.grey,
                          ),
                          SizedBox(width: 10),
                          Text(
                            'Destination, museums, tours',
                            style: TextStyle(color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 35, horizontal: 13),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Top destinations',
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                    TopDestinationImageContainers(
                      image: 'Images/dubai.jpg',
                      title: 'Dubai',
                      subTitle: '411 things to do',
                    ),
                    TopDestinationImageContainers(
                      image: 'Images/london.jpg',
                      title: 'London',
                      subTitle: '223 things to do',
                    ),
                    TopDestinationImageContainers(
                      image: 'Images/istanbul.jpg',
                      title: 'Istanbul',
                      subTitle: '161 things to do',
                    ),
                  ],
                ),
              ),
              HorizentalDivider(),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'We\'ve got you covered',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22,
                      ),
                    ),
                    SizedBox(height: 10),
                    CoveredRow(
                      iconData: Entypo.network,
                      title: 'Explore top attractions',
                      subtitle:
                          'Experience the best of your destination, with attractions, tours, activities and more',
                    ),
                    CoveredRow(
                      iconData: Icons.calendar_today_outlined,
                      title: 'Fast and flexible',
                      subtitle:
                          'Book tickets online in minutes, with free cancellation on many attractions',
                    ),
                    CoveredRow(
                      iconData: Icons.support_agent_rounded,
                      title: 'Support when you need it',
                      subtitle:
                          'Booking.com\'s global Customer Service team is here to help 24\\7',
                    ),
                  ],
                ),
              ),
              HorizentalDivider(),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Explore more destinations',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    SizedBox(height: 5),
                    Text('Find things to do in cities around the world'),
                    SizedBox(height: 25),
                    TabBar(
                      indicatorColor: Colors.blue,
                      unselectedLabelColor: Colors.black,
                      labelColor: Colors.blue,
                      onTap: (i) {
                        setState(() {
                          selectedTab = i;
                        });
                      },
                      isScrollable: true,
                      tabs: [
                        Tab(
                          child: Text(
                            'Europe',
                          ),
                        ),
                        Tab(
                          child: Text('North America'),
                        ),
                        Tab(
                          child: Text('Asia'),
                        ),
                        Tab(
                          child: Text('Africa'),
                        ),
                        Tab(
                          child: Text('Oceania'),
                        ),
                        Tab(
                          child: Text('Middle East'),
                        ),
                        Tab(
                          child: Text('Caribbean'),
                        ),
                        Tab(
                          child: Text('Central America'),
                        ),
                        Tab(
                          child: Text('South America'),
                        ),
                      ],
                    ),
                    Builder(builder: (context) {
                      if (selectedTab == 0) {
                        return Column(
                          children: [
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/istanbul.jpg',
                                title: 'Istanbul',
                                subTitle: '161 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/paris.jpg',
                                title: 'Paris',
                                subTitle: '524 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                          ],
                        );
                      } else if (selectedTab == 1) {
                        return Column(
                          children: [
                            TopDestinationImageContainers(
                                image: 'Images/newyork.jpg',
                                title: 'New York',
                                subTitle: '824 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/istanbul.jpg',
                                title: 'Istanbul',
                                subTitle: '161 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/rome.jpg',
                                title: 'Rome',
                                subTitle: '224 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                          ],
                        );
                      } else if (selectedTab == 2) {
                        return Column(
                          children: [
                            TopDestinationImageContainers(
                                image: 'Images/pakistan.jpg',
                                title: 'Pakistan',
                                subTitle: '924 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/india.jpg',
                                title: 'India',
                                subTitle: '161 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                          ],
                        );
                      } else {
                        return Column(
                          children: [
                            TopDestinationImageContainers(
                                image: 'Images/pakistan.jpg',
                                title: 'Pakistan',
                                subTitle: '924 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/india.jpg',
                                title: 'India',
                                subTitle: '161 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                            TopDestinationImageContainers(
                                image: 'Images/london.jpg',
                                title: 'London',
                                subTitle: '224 things to do'),
                          ],
                        );
                      }
                    }),
                  ],
                ),
              ),
              SizedBox(height: 30),
              Container(
                padding: EdgeInsets.symmetric(vertical: 15),
                width: double.infinity,
                color: Colors.blue[900],
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'About Booking.com',
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                        SizedBox(width: 20),
                        Text(
                          'Terms & Conditions',
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      ],
                    ),
                    SizedBox(height: 15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Privacy & Cookie Statement',
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                        SizedBox(width: 10),
                        Text(
                          'Attractions help centre',
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      ],
                    ),
                    SizedBox(height: 40),
                    Text(
                      'Copyright 1996-2021 Booking.com. All rights reserved',
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
