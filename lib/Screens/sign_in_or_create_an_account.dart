import 'package:booking_app/Components/sign_in_container.dart';
import 'package:booking_app/Screens/navigator_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/brandico_icons.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';

class SignInOrCreateAccount extends StatelessWidget {
  const SignInOrCreateAccount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) {
              return NavigatorScreen();
            }));
          },
        ),
        backgroundColor: Colors.blue[900],
        title: RichText(
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(
                  text: 'Book',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              TextSpan(
                  text: 'inz',
                  style: TextStyle(
                      color: Colors.blue[400],
                      fontSize: 20,
                      fontWeight: FontWeight.bold)),
            ],
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Sign in or create an account',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
              SizedBox(height: 10),
              Text(
                  'Choose any of the options below to sign in or start creating an account.'),
              SizedBox(height: 20),
              SignInContainers(
                color: Colors.blue[700],
                title: "Continue with email",
              ),
              SignInContainers(
                title: "Sign in with Google",
                icon: Icon(
                  FontAwesome.google,
                  color: Colors.red[500],
                ),
              ),
              SignInContainers(
                color: Colors.blue[700],
                title: "Sign in with Facebook",
                icon: Icon(
                  Brandico.facebook_rect,
                  color: Colors.white,
                ),
              ),
              Spacer(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                      'By logging in or creating an account, you agree with our'),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.black, fontSize: 13.5),
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Terms & Conditions ',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: Color(0xff0077B5),
                          ),
                        ),
                        TextSpan(text: 'and '),
                        TextSpan(
                          text: 'Privacy Statement.',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: Color(0xff0077B5),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Don\'t sell my personal information',
                    style: TextStyle(
                        color: Color(0xff0077B5), fontWeight: FontWeight.w500),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
