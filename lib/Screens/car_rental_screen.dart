import 'package:booking_app/Components/find_out_how_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';

class CarRentalScreen extends StatefulWidget {
  const CarRentalScreen({Key? key}) : super(key: key);

  @override
  _CarRentalScreenState createState() => _CarRentalScreenState();
}

class _CarRentalScreenState extends State<CarRentalScreen> {
  bool _switchValue = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            FindOutHowContainer(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 30),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text('Return to same location'),
                      Spacer(),
                      Text(_switchValue ? 'Yes' : 'No'),
                      Transform.scale(
                        scale: 0.7,
                        child: CupertinoSwitch(
                          activeColor: Colors.blue[700],
                          value: _switchValue,
                          onChanged: (bool value) {
                            setState(() {
                              setState(() {
                                _switchValue = value;
                              });
                            });
                          },
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 20),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.white,
                        border: Border.all(
                            color: Colors.orange.withOpacity(0.6), width: 4)),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8, vertical: 15),
                          child: Row(
                            children: [
                              Icon(
                                Icons.local_taxi_outlined,
                                color: Colors.grey,
                                size: 20,
                              ),
                              SizedBox(width: 5),
                              Text(
                                'Pick-up location',
                                style: TextStyle(color: Colors.grey),
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 4,
                          color: Colors.orange.withOpacity(0.6),
                        ),
                        _switchValue != true
                            ? Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 15),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.local_taxi_outlined,
                                      color: Colors.grey,
                                      size: 20,
                                    ),
                                    SizedBox(width: 5),
                                    Text(
                                      'Drop-off location',
                                      style: TextStyle(color: Colors.grey),
                                    )
                                  ],
                                ),
                              )
                            : SizedBox(),
                        _switchValue != true
                            ? Container(
                                width: double.infinity,
                                height: 4,
                                color: Colors.orange.withOpacity(0.6),
                              )
                            : SizedBox(),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8, vertical: 15),
                          child: Row(
                            children: [
                              Icon(
                                Icons.calendar_today_rounded,
                                size: 20,
                              ),
                              SizedBox(width: 5),
                              Text(
                                'Thu 11 Nov',
                              ),
                              Spacer(),
                              Text('10.00 am'),
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 4,
                          color: Colors.orange.withOpacity(0.6),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8, vertical: 15),
                          child: Row(
                            children: [
                              Icon(
                                Icons.calendar_today_rounded,
                                size: 20,
                              ),
                              SizedBox(width: 5),
                              Text(
                                'Sun 14 Nov',
                              ),
                              Spacer(),
                              Text('10.00 am'),
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 4,
                          color: Colors.orange.withOpacity(0.6),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8, vertical: 15),
                          child: Row(
                            children: [
                              Icon(
                                Icons.person_outline,
                                color: Colors.grey,
                                size: 20,
                              ),
                              SizedBox(width: 5),
                              Text(
                                'Driver\'s age: 30-65',
                                style: TextStyle(color: Colors.grey),
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: 4,
                          color: Colors.orange.withOpacity(0.6),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 17),
                          alignment: Alignment.center,
                          color: Colors.blue[700],
                          child: Text(
                            'Search',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 17, horizontal: 15),
              child: Row(
                children: [
                  Icon(
                    Icons.local_taxi_outlined,
                    size: 20,
                  ),
                  SizedBox(width: 15),
                  Text('View and manage your car booking'),
                  Spacer(),
                  Icon(
                    Icons.arrow_forward_ios_sharp,
                    size: 18,
                    color: Colors.grey.withOpacity(0.4),
                  )
                ],
              ),
            ),
            SizedBox(height: 20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: double.infinity,
                ),
                Text(
                  'Terms and condition',
                  style: TextStyle(
                      color: Colors.blue, fontWeight: FontWeight.w400),
                ),
                SizedBox(height: 30),
                Text('Privacy policy',
                    style: TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.w400)),
                SizedBox(height: 30),
                Text('Contact us',
                    style: TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.w400)),
                SizedBox(height: 30)
              ],
            )
          ],
        ),
      ),
    );
  }
}
