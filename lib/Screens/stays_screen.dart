import 'package:booking_app/Components/custom_divider.dart';
import 'package:booking_app/Components/levels_container.dart';
import 'package:booking_app/Components/lock_level_container.dart';
import 'package:booking_app/Components/stays_container_row.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';

class StaysScreen extends StatefulWidget {
  const StaysScreen({Key? key}) : super(key: key);

  @override
  _StaysScreenState createState() => _StaysScreenState();
}

class _StaysScreenState extends State<StaysScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  top: 20, right: 20, left: 20, bottom: 15),
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(
                          color: Colors.orange.withOpacity(0.6), width: 4),
                    ),
                    child: Column(
                      children: [
                        StaysContainerRow(
                          title: 'Enter your destination',
                          iconData: Icons.search,
                          suffixIcon: Icons.mic_none,
                        ),
                        Container(
                          width: double.infinity,
                          height: 4,
                          color: Colors.orange.withOpacity(0.6),
                        ),
                        StaysContainerRow(
                          title: 'Wed 27 Oct - Thu 28 Oct',
                          iconData: Icons.calendar_today_sharp,
                        ),
                        Container(
                          width: double.infinity,
                          height: 4,
                          color: Colors.orange.withOpacity(0.6),
                        ),
                        StaysContainerRow(
                          title: '1 room, 2 adults, 0 children',
                          iconData: Icons.person_outline,
                        ),
                        Container(
                          width: double.infinity,
                          height: 4,
                          color: Colors.orange.withOpacity(0.6),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 17),
                          alignment: Alignment.center,
                          color: Colors.blue[700],
                          child: Text(
                            'Search',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 15),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        border: Border.all(
                            color: Colors.grey.withOpacity(0.4), width: 1)),
                    child: Row(
                      children: [
                        Icon(
                          Icons.warning_amber_outlined,
                          size: 20,
                          color: Colors.orangeAccent,
                        ),
                        SizedBox(width: 15),
                        Text('Coronavirus (COVID-19) Support'),
                        Spacer(),
                        Icon(
                          Icons.arrow_forward_ios_sharp,
                          size: 15,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            CustomDivider(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Travel more,spend less',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  SizedBox(height: 20),
                  Container(
                    height: 120,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        LevelsContainer(),
                        LevelsContainer(),
                        LockLevelsContainer(),
                        LockLevelsContainer(),
                        LockLevelsContainer(),
                        LockLevelsContainer(),
                      ],
                    ),
                  )
                ],
              ),
            ),
            CustomDivider(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'More for you',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 325,
                            width: 165,
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 8),
                            alignment: Alignment.bottomLeft,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(0, 0),
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 1,
                                )
                              ],
                              image: DecorationImage(
                                  image: AssetImage('Images/1.jpg'),
                                  fit: BoxFit.cover),
                            ),
                            child: Text(
                              'Quick trips',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Column(
                            children: [
                              Container(
                                height: 265,
                                width: 165,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5)),
                                  boxShadow: [
                                    BoxShadow(
                                      offset: Offset(0, 0),
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 1,
                                      blurRadius: 1,
                                    )
                                  ],
                                  image: DecorationImage(
                                      image: AssetImage('Images/4.jpg'),
                                      fit: BoxFit.cover),
                                ),
                              ),
                              Container(
                                width: 165,
                                padding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 5),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        offset: Offset(0, 0),
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 1,
                                      )
                                    ],
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(5),
                                        bottomRight: Radius.circular(5))),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Travel talk',
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 12),
                                    ),
                                    Text(
                                      'Pakistan',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(width: 20),
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            height: 210,
                            width: 165,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Icon(
                                  Icons.shopping_cart_outlined,
                                  color: Colors.orange,
                                ),
                                Text(
                                  'Save at least 15%',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                                Text(
                                  'it\'s not too late to travel, with Gateway Deals on stays all through 2021',
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 15),
                                ),
                              ],
                            ),
                            decoration:
                                BoxDecoration(color: Colors.white, boxShadow: [
                              BoxShadow(
                                  offset: Offset(0, 0),
                                  color: Colors.grey.withOpacity(0.2),
                                  spreadRadius: 1,
                                  blurRadius: 1)
                            ]),
                          ),
                          SizedBox(height: 10),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 7),
                            height: 210,
                            width: 165,
                            alignment: Alignment.bottomLeft,
                            child: Text(
                              'Travel talk',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('Images/2.jpg'),
                                  fit: BoxFit.cover),
                            ),
                          ),
                          SizedBox(height: 10),
                          Container(
                            height: 210,
                            width: 165,
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 7),
                            alignment: Alignment.bottomLeft,
                            child: Text(
                              'Travel talk',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('Images/3.jpg'),
                                  fit: BoxFit.cover),
                            ),
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
