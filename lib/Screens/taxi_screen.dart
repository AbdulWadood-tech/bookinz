import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TaxiScreen extends StatefulWidget {
  const TaxiScreen({Key? key}) : super(key: key);

  @override
  _TaxiScreenState createState() => _TaxiScreenState();
}

class _TaxiScreenState extends State<TaxiScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Find the taxi for your trip',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            SizedBox(height: 10),
            Text('Travel with ease - no queues, no cah, no fees'),
            SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  color: Colors.white,
                  border: Border.all(
                      color: Colors.orange.withOpacity(0.6), width: 4)),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8, top: 15),
                    child: Row(
                      children: [
                        Icon(
                          Icons.panorama_fisheye_sharp,
                          color: Colors.grey,
                          size: 17,
                        ),
                        SizedBox(width: 5),
                        Text(
                          'Enter Pick-up location',
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 35,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      children: [
                        Container(
                          height: 35,
                          width: 1.5,
                          color: Colors.grey,
                        ),
                        SizedBox(width: 20),
                        Expanded(
                          child: Container(
                            height: 1,
                            color: Colors.grey.withOpacity(0.4),
                          ),
                        ),
                        Icon(Icons.height)
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 8, right: 8, bottom: 15),
                    child: Row(
                      children: [
                        Icon(
                          Icons.panorama_fisheye_sharp,
                          color: Colors.grey,
                          size: 17,
                        ),
                        SizedBox(width: 5),
                        Text(
                          'Enter destination',
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 4,
                    color: Colors.orange.withOpacity(0.6),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 15),
                    child: Row(
                      children: [
                        Icon(
                          Icons.calendar_today_rounded,
                          size: 20,
                        ),
                        SizedBox(width: 5),
                        Text(
                          'Sun 14 Nov, 03:50 pm',
                        ),
                        Spacer(),
                        Icon(Icons.code),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 4,
                    color: Colors.orange.withOpacity(0.6),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 15),
                    child: Row(
                      children: [
                        Icon(
                          Icons.add,
                          color: Colors.grey,
                          size: 20,
                        ),
                        SizedBox(width: 5),
                        Text(
                          'Need a return?',
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 4,
                    color: Colors.orange.withOpacity(0.6),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 17),
                    alignment: Alignment.center,
                    color: Colors.blue[700],
                    child: Text(
                      'See Prices',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
