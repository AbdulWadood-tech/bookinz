import 'package:booking_app/Components/horizental_divider.dart';
import 'package:booking_app/Components/list_options.dart';
import 'package:booking_app/Components/menu_text.dart';
import 'package:booking_app/Components/sign_button.dart';
import 'package:booking_app/pages/genius_loyalty_programme.dart';
import 'package:booking_app/pages/rewards_wallets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:im_stepper/stepper.dart';

import 'bookings_screen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool userExist = true;
  int activeStep = 0;
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        elevation: 0.0,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Icon(FontAwesome.question_circle_o),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 325,
              child: Stack(
                children: [
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      alignment: Alignment.center,
                      color: Colors.blue[900],
                      padding: EdgeInsets.only(bottom: 50),
                      child: Column(
                        children: [
                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.orange.withOpacity(0.9),
                                    width: 2),
                                color: Colors.grey,
                                image: DecorationImage(
                                    image: AssetImage('Images/1.jpg')),
                                borderRadius: BorderRadius.circular(50)),
                          ),
                          SizedBox(height: 20),
                          userExist
                              ? Text(
                                  'Abdul Wadood',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontSize: 22),
                                )
                              : Text(
                                  'Sign in to see deals and Genius discounts,\nmanage your trips, and more',
                                  style: TextStyle(color: Colors.yellow[700]),
                                  textAlign: TextAlign.center,
                                ),
                          SizedBox(height: 10),
                          userExist
                              ? Text(
                                  'Genius Level 1',
                                  style: TextStyle(
                                    color: Colors.yellow[700],
                                  ),
                                )
                              : SignInButton(),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 30,
                    right: 30,
                    child: Container(
                      height: 170,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border:
                              Border.all(color: Colors.grey.withOpacity(0.3))),
                      child: Column(
                        children: [
                          IconStepper(
                            stepPadding: 0,
                            icons: [
                              Icon(Icons.supervised_user_circle),
                              Icon(Icons.flag),
                              Icon(Icons.access_alarm),
                              Icon(Icons.supervised_user_circle),
                              Icon(Icons.flag),
                              Icon(Icons.access_alarm),
                              Icon(Icons.supervised_user_circle),
                            ],

                            // activeStep property set to activeStep variable defined above.
                            activeStep: activeStep,

                            // This ensures step-tapping updates the activeStep.
                            onStepReached: (index) {
                              setState(() {
                                activeStep = index;
                              });
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: HorizentalDivider(),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 13, vertical: 15),
                            child: Row(
                              children: [
                                Text(
                                  'Bookinz\'s loyalty programme',
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                                Spacer(),
                                Text(
                                  'Genius',
                                  style: TextStyle(
                                      color: Colors.blue[900],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30),
            ListOptions(
              title: userExist
                  ? 'Manage your account'
                  : 'Sign in or create account',
              iconData: userExist ? Icons.person_outline : Icons.exit_to_app,
              onClick: () {},
            ),
            ListOptions(
              title: 'Rewards \$ Wallet',
              iconData: Icons.file_copy,
              onClick: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RewardWallet()));
              },
            ),
            ListOptions(
              title: 'Gift cards',
              iconData: Icons.wallet_giftcard_sharp,
              onClick: () {},
            ),
            ListOptions(
              title: 'Genius loyalty programme',
              iconData: FontAwesome.google,
              onClick: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => GeniusLoyaltyProgramme()));
              },
            ),
            ListOptions(
              title: 'Reviews',
              iconData: Icons.reviews,
              onClick: () {},
            ),
            ListOptions(
              title: 'Questions to properties',
              iconData: Icons.file_copy_outlined,
              onClick: () {},
            ),
            MenuText(
              title: 'Help and support',
            ),
            ListOptions(
              title: 'Contacts Customer Service',
              iconData: FontAwesome.question_circle_o,
              onClick: () {},
            ),
            ListOptions(
              title: 'Safety resource centre',
              iconData: Icons.camera,
              onClick: () {},
            ),
            ListOptions(
              title: 'Dispute resolution',
              iconData: Icons.sports_handball_outlined,
              onClick: () {},
            ),
            MenuText(
              title: 'Discover',
            ),
            ListOptions(
              title: 'Deals',
              iconData: Icons.accessibility_new_sharp,
              onClick: () {},
            ),
            ListOptions(
              title: 'Airport Taxis',
              iconData: Icons.airplanemode_on_sharp,
              onClick: () {},
            ),
            ListOptions(
              title: 'Travel articles',
              iconData: Icons.airplanemode_on_sharp,
              onClick: () {},
            ),
            ListOptions(
              title: 'Travel communities',
              iconData: Icons.airplanemode_on_sharp,
              onClick: () {},
            ),
            MenuText(title: 'Settings and legal'),
            ListOptions(
              title: 'Settings',
              iconData: Icons.settings,
              onClick: () {},
            ),
            ListOptions(
              title: 'Give app feedback',
              iconData: Icons.airplanemode_on_sharp,
              onClick: () {},
            ),
            ListOptions(
              title: 'Legal',
              iconData: Icons.airplanemode_on_sharp,
              onClick: () {},
            ),
            MenuText(title: 'Partners'),
            ListOptions(
              title: 'List your property',
              iconData: Icons.airplanemode_on_sharp,
              onClick: () {},
            ),
            ListOptions(
              title: 'Sign Out',
              iconData: Icons.logout,
              onClick: () {},
              check: true,
            ),
          ],
        ),
      ),
    );
  }
}
