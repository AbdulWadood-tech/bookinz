import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StaysContainerRow extends StatelessWidget {
  final String title;
  final IconData iconData;
  final IconData? suffixIcon;
  const StaysContainerRow({
    Key? key,
    required this.iconData,
    required this.title,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
      child: Row(
        children: [
          Icon(
            iconData,
            size: 22,
          ),
          SizedBox(width: 10),
          Text(title),
          Spacer(),
          Icon(
            suffixIcon,
            color: Colors.blue,
          ),
        ],
      ),
    );
  }
}
