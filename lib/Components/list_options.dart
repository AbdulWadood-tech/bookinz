import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListOptions extends StatelessWidget {
  final String title;
  final IconData iconData;
  final bool check;
  final VoidCallback onClick;
  const ListOptions({
    Key? key,
    this.check = false,
    required this.iconData,
    required this.title,
    required this.onClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 20),
      child: InkWell(
        onTap: onClick,
        child: Row(
          children: [
            Icon(
              iconData,
              size: 20,
              color: check ? Colors.deepOrange : Colors.black,
            ),
            SizedBox(width: 10),
            Text(
              title,
              style: TextStyle(
                  fontSize: 14,
                  color: check ? Colors.deepOrange : Colors.black),
            ),
          ],
        ),
      ),
    );
  }
}
