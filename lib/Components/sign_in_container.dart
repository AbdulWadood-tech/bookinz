import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignInContainers extends StatelessWidget {
  final String title;
  final Color? color;
  final Icon? icon;
  const SignInContainers({
    Key? key,
    this.icon,
    required this.title,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Container(
        alignment: Alignment.center,
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(3),
            color: color ?? Colors.white,
            border: Border.all(
                color: color == null ? Colors.black : Colors.transparent,
                width: 1)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon ?? SizedBox(),
            SizedBox(
              width: 10,
            ),
            Text(
              title,
              style:
                  TextStyle(color: color == null ? Colors.black : Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
