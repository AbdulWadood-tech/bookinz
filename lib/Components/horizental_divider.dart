import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HorizentalDivider extends StatelessWidget {
  const HorizentalDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 1,
      color: Colors.grey.withOpacity(0.2),
    );
  }
}
