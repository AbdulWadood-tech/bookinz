import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';

class LockLevelsContainer extends StatelessWidget {
  const LockLevelsContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
        width: 250,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey.withOpacity(0.5), width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '10% discounts',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Icon(
                  Icons.lock_outlined,
                  color: Colors.grey,
                  size: 20,
                )
              ],
            ),
            Spacer(),
            Text(
              'Complete 5 stays to unlock Genius Level 2',
              style: TextStyle(color: Colors.grey),
            )
          ],
        ),
      ),
    );
  }
}
