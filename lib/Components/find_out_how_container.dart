import 'package:flutter/material.dart';

class FindOutHowContainer extends StatelessWidget {
  const FindOutHowContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.orange.withOpacity(0.1),
          border: Border.all(color: Colors.orange)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            Icons.warning_amber_outlined,
            color: Colors.deepOrange[900],
          ),
          SizedBox(width: 10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Clean cars, Flexible bookings. Socially distant rental counters',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                SizedBox(height: 10),
                Text(
                  'We\'re working with our partners to keep you safe and in the driving seat',
                  style: TextStyle(fontSize: 15),
                ),
                SizedBox(height: 15),
                Text(
                  'Find out how',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.blue[700],
                      fontSize: 17),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
