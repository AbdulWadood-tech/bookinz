import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoveredRow extends StatelessWidget {
  final IconData iconData;
  final String title;
  final String subtitle;
  const CoveredRow({
    Key? key,
    required this.iconData,
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            iconData,
            color: Colors.green,
          ),
          SizedBox(width: 10),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(subtitle),
            ],
          ))
        ],
      ),
    );
  }
}
